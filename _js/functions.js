var time = 100; // Tempo de digitação em mili segundos
var atual = 0; // indice atual do bloco de texto
var qtd; //Definimos uma variavel
 
$(function(){       
    $('.textosTroca .item').hide();        
    qtd = $('.textosTroca .item').length;       
    setTimeout('digitaTexto('+atual+')',500);   

    //notepad
    $("#notepad").focusout(function() {
        localStorage.setItem('dados', $("#notepad").html());
     });

    if (localStorage.getItem('dados')) {
        $("#notepad").html(localStorage.getItem('dados'));
    }

        $("#limpar").click(function() {
        localStorage.clear();
        window.location = window.location;
    });
});   
 
function digitaTexto(item){
    $('.areaTexto').html('');
    objItem = $('.textosTroca .item').eq(item);        
    texto = ($.trim(objItem.html()));       
    if(!objItem.attr('texto')){           
        objItem.attr('texto',texto);            
        objItem.html('');       
    }               
    stringItem = new String(objItem.attr('texto'));               
    setTimeout('efeitoDigita(stringItem,0)',time);   
} 
 
function efeitoDigita(stringItem,n){       
    qtdLetras = stringItem.length;        
    $('.areaTexto').append(stringItem[n]);               
    if((n+1) < qtdLetras){           
        setTimeout('efeitoDigita(stringItem,'+(n+1)+')',time);       
    }else{           
        if((atual+1) < qtd){               
            atual++           
        }else{               
            atual = 0;           
        }           
        setTimeout('digitaTexto('+atual+')',3000);       
    }           
}