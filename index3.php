<?php
session_start();
include("Mail.php");

if($_GET) {
	if( $_GET['acao'] == "limpar" ) { unset($_SESSION['out']); }
	if( $_GET['acao'] == "limparlast" ) { unset($_SESSION['last']); }
}

if( $_POST ) {
	if($_POST['senha'] == "teste") {		

		if( (!empty($_POST['nome_out'])) && (!empty($_POST['tipo_envio'])) ) {

			if( $_SESSION["out"] != $_POST['nome_out'] ) {

				//$recipients 				= 'demissoesrh@trip.com.br';
				$recipients 				= 'testechamados@trip.com.br';
				
				$headers['From']    		= 'fabio@trip.com.br';
				$headers['To']      		= 'fabio@trip.com.br';
				$headers['MIME-Version']	= '1.0';
				$headers["Content-type"]	= "text/html; charset=UTF-8";
				$headers['Subject'] 		= 'Test message';
				$nomes 						= 	$_POST['nome_out'];		
				$_SESSION["out"] 			=	$nomes;
				$_SESSION["last"]			=	$nomes;
				$tipo_envio					= 	$_POST['tipo_envio'];

				//Parametros SMTP
				$params["host"] 			= "tripmail.trip.com.br";
				$params["port"] 			= "25"; 
				$params["auth"] 			= true; 
				$params["username"] 		= "fabio@trip.com.br"; 
				$params['password']			= 'password765';

				$smtp = Mail::factory('smtp', $params);	

//////////////////////////////////////////////////////////
//// MENSAGENS PARA DEMISSAO
/////////////////////////////////////////////////////////				
				/****************************************
				//SUPORTE TI
				*****************************************/
				$depts['d']['suporte_ti']['dep']			= "Suporte TI";
				$depts['d']['suporte_ti']['destino'] 	= "suporte@trip.com.br";
				$depts['d']['suporte_ti']['assuntos']	=	array(
														"computador"		=>	"- Retirar computador",
														"ramal"				=>	"- Retirar ramal",
														"iPad"				=>	"- Retirar iPad",
														"e-mail"			=>	"- Trocar senha de acesso ao e-mail <br />
																				 - Configurar resposta automática do e-mail <br />
																				 - Configurar encaminhamento do e-mail <br />
																				 - Agendar exclusão da conta do e-mail e anti-spam.",
														"AD"				=>	"- Bloquear acesso AD",
														"VPN"				=>	"- Bloquear acesso VPN",
														"Chamados"			=>	"- Bloquear acesso sistema chamados",
														"Banco de imagens"	=>	"- Deletar acesso sistema banco de imagens",
														"WoodWing"			=>	"- Deletar acesso sistema WoodWing",
														"Software"			=>	"- Retirar licença de softwares",
														"Mac Address"		=>	"- Apagar cadastro Mac Adress"
													);

				/****************************************
				//SUPORTE TRANSPORTES
				*****************************************/
				$depts['d']['transportes']['dep']		 = "Transportes";
				$depts['d']['transportes']['destino']	 = "transportes@trip.com.br";
				$depts['d']['transportes']['assuntos']	 =	array( 
														"Celular"			=>	"- Retirar celular"
														);

				/****************************************
				//SUPORTE PROTHEUS
				*****************************************/
				$depts['d']['protheus']['dep']		=	"Suporte Protheus";
				$depts['d']['protheus']['destino']	=	"suporteprotheus@trip.com.br";
				$depts['d']['protheus']['assuntos']	=	array(
														"Protheus"			=>	"- Deletar acesso Protheus",
														"BI"				=>	"- Deletar acesso BI",
														"Jurídico"			=>	"- Deletar acesso sistema jurídico"
													);

				/****************************************
				//DESENVOLVIMENTO WEB
				*****************************************/
				$depts['d']['dev_web']['dep']			=	"Desenvolvimento Web";
				$depts['d']['dev_web']['destino']		=	"desenvolvimentoweb@trip.com.br";
				$depts['d']['dev_web']['assuntos']		=	array(
														"Sistemas"			=>	"Deletar acesso sistemas: <br />
																				 - Project <br />
																				 - Intranet <br />
																				 - CMS - Trip <br />
																				 - CMS - Tpm <br />
																				 - CMS - Transformadores <br />
																				 - CMS - Institucional <br />
																				 - CMS - Mailing <br />
																				 - CMS - Assinaturas"
													);

				/****************************************
				//RH
				*****************************************/
				$depts['d']['rh']['dep']				=	"RH[Recursos Humanos]";
				$depts['d']['rh']['destino']			=	"rh@trip.com.br";
				$depts['d']['rh']['assuntos']			= 	array(
																"Ramais"	=>	"- Deletar cadastro na lista de ramais intranet"
															);

//////////////////////////////////////////////////////////
//// FIM MENSAGENS PARA DEMISSAO
/////////////////////////////////////////////////////////				

//////////////////////////////////////////////////////////
//// MENSAGENS PARA CONFIRMAR
/////////////////////////////////////////////////////////				

				//RH - RECURSOS HUMANOS
				$depts['c']['rh']['dep']				=	"RH[Recursos Humanos]";
				$depts['c']['rh']['destino']			=	"rh@trip.com.br";
				$depts['c']['rh']['assuntos']			=	array(
																"Confirmar Demissão"	=>	"Confirmar demissão"
															);


/////////////////////////////////////////////////////////
//// FIM MENSAGENS PARA CONFIRMAR
/////////////////////////////////////////////////////////				

				/*****************************************
				// Enviando
				*****************************************/
				$log = "<h2>Ações - E-mails enviados</h2>";

				$nomes = explode(",",$nomes);
				
				//verifica qual array irá enviar
				if( $tipo_envio == "confirmar" ) 	{ $deptos = $depts['c']; }
				if( $tipo_envio == "demitir" ) 		{ $deptos = $depts['d']; }

				foreach($nomes as $nome) {
					//manipulando a variavel nome recebida
					$nome = explode("[", $nome);					
					$nome_email["aka"] = trim($nome[0]);
					$nome = str_replace("]", "", $nome[1]);
					$nome = explode("-", $nome);
					$nome_email["nome_comp"] = trim($nome[0]);
					$nome_email["cargo"] = trim($nome[1]);

					if(trim($nome_email['aka'])) {
						$log .="<h2>".$nome_email['aka']."</h2><hr>";
						foreach( $deptos as $dept ) {
							//$recipients	= $dept['destino'];
							$log .= "<h3>".$dept['dep']."</h3><ol>";

							foreach( $dept['assuntos'] as $assunto => $corpo ) {
								$headers['Subject'] = 	"[D] ".$nome_email['aka']." - ".$assunto;
								$body				=	$nome_email['nome_comp']." - ". $nome_email['cargo'] ."<br /><br />".$corpo;
								$log .= "<li>".$headers['Subject']."</li>";

								$mail = $smtp->send($recipients, $headers, $body);

								if($mail) { $log .= "<p>enviado</p>"; }
							}
							$log .= "</ol>";
						}		
						
					}					
				}
				//exit();

				

			}	else {

				$log = "
					<h2>Não Enviado</h2>
					<p><strong>Motivo:</strong> Você já enviou um e-mail com o nome ".$_POST['nome_out']."</p>
					<p>Caso queira reenviar, <a href=\"?acao=limpar\">clique aqui</a></p>
					";
			}

		} else {

			$erro = "<p class=\"erro\">Necessário informar um nome e tipo de envio.</p>";
		}
		

	} else {

		$erro = "<p class=\"erro\">Senha incorreta!</p>";

	}
}


?>

<!doctype html>
<html lang="pt-br">	
	
	<head>		
		<title>Trip Out</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="_css/style.css">
		<script src="_js/jquery-1.9.0.js"></script>
		<script src="_js/functions.js"></script>
	</head>

	<body>
		<div id="container">
			<h1>OUT - Organization Underground Trip</h1>
			<?php if($log) { ?>

			<div id="box_form">								
				<?php echo $log; ?>				
			</div>
			<div class="box_texto">
				<div class="mascara">
					<?php
						$besteirou = explode(" ", $nome);
					?>
					<span class="mask"><?php echo $besteirou[0]; ?> está <br /><strong>demitido</strong></span>
					<img src="_img/out.jpg" style="margin-bottom: 20px;" />
				</div>
				<a href="index3.php" class="voltar">Demitir mais alguém</a>
			</div>

			<?php } else { ?>

			<div id="box_form">
				<form method="post" action="?" id="form_out" >
					<textarea name="nome_out" value="" placeholder="Nome do(s) funcionário(s)"></textarea>
					<input type="password" name="senha" placeholder="Senha" />
					<label for="tipo_envio">Tipo de envio:</label>
					<select name="tipo_envio">
						<option value="">--Escolha a opção--</option>
						<option value="confirmar">CONFIRMAR</option>
						<option value="demitir">DEMITIR</option>
					</select>
					<button type="submit">Enviar</button>
				</form>
			</div>
			<div class="box_texto">Digite o nome da pessoa a ser desligada!<?php echo $erro; ?>
				<?if($_SESSION['last']){?>
				<h2>Ultimos Enviados</h2>
				<textarea style="width:100%"><?php echo $_SESSION['last']; ?></textarea>
				<a href="?acao=limparlast">Limpar nomes</a>
				<?}?>
			</div>			
			
			<?php } ?>
		</div>
	</body>

</html>